## Repozytorium do poćwiczenia gita :)

### Lista tasków:

1. Sklonuj to repozytorium ( pobierz, jeżeli na windowsie masz włączone [pokazywanie ukrytych plików i folderów](https://www.technipages.com/wp-content/uploads/2011/08/Win7-show-hidden-files-option.png) (polecam włączyć),
zobaczysz w nim folder .git

2. Wejdź na brancha master (to powinno stać się automatycznie), stwórz nowego brancha (w nazwie najlepiej dodać swoje imię)
[W intellij rzeczy związane z branchami są w prawym, dolnym rogu](https://d3nmt5vlzunoa1.cloudfront.net/idea/files/2016/01/git-operations.png)

3. Stwórz nowy plik w folderze z repozytorium na lokalnym komputerze, dodaj plik do gita (Intellij powinien spytać czy dodać plik, jeżeli został on stworzony w obrębie IDE)
(Gdyby plik świecił sie na czerwono, a nie na zielono, kliknij na niego PPM -> Git -> Add (Lub Ctrl + Alt + A mając zaznaczony plik))

4. Zrób commit, w okienku mając zaznaczony plik który chcesz zcommitować 
(Większość operacji związanych z gitem, jest w górnej zakładce VCS (version control system) w Intellij)

5. Zrób push swojego brancha (z nowym commitem) do zdalnego repozytorium
(Można w poprzednim kroku zrobić od razu commit + push z poziomu Intellij)

6. Zrób nowego brancha, z Twojego obecnego brancha, oba branche będą identyczne

7. Wróc na pierwszego brancha

8. Poczekaj :) kiedy każdy będzie miał swojego brancha, pushnę commita na mastera, tak aby historia naszych branchy się różniła.

9. Na PIERWSZYM branchu zrób rebase na zaktualizowanego mastera, sprawdź historię swojego brancha (można ją sprawdzić klikając Alt + 9 w Intellij -> Log -> wybrać brancha) 

10. Na DRUGIM brancha zrób merge z masterem, sprawdź historię swojego brancha (będzie widać różnice między merge a rebase)

11. PIERWSZEGO brancha, który był rebasowany, trzeba będzie FORCE PUSHNĄĆ, zrób to

12. DRUGIEGO brancha, który był mergowany, wystarczy tylko pushnąć, bo nie było go nigdy poza Twoim lokalnym repozytorium

13. Na GitLabie, z lewej strony wybierz Merge Requests, następnie wybierz swojego PIERWSZEGO brancha, tak aby zrobić MERGE REQUEST do mastera, jako Assignee wybierz mnie :) (@oskar.szwajkowski)

14. Hura, umiesz w gita :)